\documentclass[ngerman, a4paper, numbers=noenddot, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{lmodern}
\usepackage[top=2cm, right=2cm, bottom=2.5cm, left=3.5cm]{geometry}
\usepackage{calc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage[official]{eurosym}
\usepackage{lastpage}
\usepackage{etoolbox}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{relsize}
\usepackage{natbib}
\usepackage[printonlyused,withpage]{acronym}
\bibliographystyle{alpha}
\renewcommand{\familydefault}{\sfdefault}


% ------------- Abbildungen mit Tikz erstellen
\usepackage{tikz}
\usetikzlibrary{arrows,automata,shapes,calc}


% ------------- Zeilenabstand
\usepackage{setspace}
\onehalfspacing

% ------------- Kopfzeile
\usepackage[automark]{scrlayer-scrpage}
\pagestyle{scrheadings}
\clearscrheadings
\ihead{}
%\ihead[\rightmark]{\pagemark}
\chead{}
%\chead{}{}
\ohead{\pagemark}
%\ohead[\pagemark]{\rightmark}


\usepackage[hidelinks]{hyperref}


\newcommand{\lnameref}[1]{%
	\bgroup
	\let\nmu\MakeLowercase
	\nameref{#1}\egroup}
\newcommand{\fnameref}[1]{%
	\bgroup
	\def\nmu{\let\nmu\MakeLowercase}%
	\nameref{#1}\egroup}


\newcommand{\nmu}{}

%-------------- Ende allgemeine Angaben -----------------------------
\begin{document}
	
	%-------------- Beginn Titelseite -----------------------------------
	\hypersetup{pageanchor=false}
	\thispagestyle{empty}
	\includegraphics[width=0.93\textwidth]{THM_Logo_4c.png}
	
	\vspace{1cm}
	\begin{center}
		\vspace{2cm}
		{\Huge Ensemble methods in Bioinformatics}\\
		
		\vspace{3cm}
		Advisor: Fabian Tann\\
	\end{center}
	
	\vspace{3cm}
	\begin{flushright}
		{Georg Grünert, 5162846}\\
		{Seminar Bioinformatics Winter term 2022/2023}\\
		
		\vspace{1cm}
		\today
	\end{flushright}
	%-------------- Ende Titelseite -------------------------------------
	
	%-------------- Inhaltsverzeichnis
	\newpage
	\hypersetup{pageanchor=true}
	\pagenumbering{Roman}
	\renewcommand{\thepage}{\Roman{page}}
	\tableofcontents{}
	
	%-------------- Abbildungsverzeichnis
	\newpage
	\cleardoublepage\phantomsection
	\addcontentsline{toc}{section}{List of Figures}
	\listoffigures
	
	
	%-------------- Tabellenverzeichnis
	\newpage
	\cleardoublepage\phantomsection
	\addcontentsline{toc}{section}{List of Tables}
	\listoftables
	
	\newpage
	\cleardoublepage\phantomsection
	\addcontentsline{toc}{section}{List of Acronyms}
	\section*{List of Acronyms}
	\begin{acronym}
		\acro{RA}{rheumatoid arthritis}
		\acro{IBD}{inflammatory bowel disease}
		\acro{DT}{decision tree}
		\acro{ET}{extra trees}
		\acro{RF}{random forest}
		\acro{ML}{machine learning}
		\acro{NN}{neural network}
		\acro{k-NN}[\emph{k}-NN]{\emph{k}-nearest-neighbour}
		\acro{SVM}{support vector machine}
		\acro{DL}{deep learning}
		\acro{RNA}{ribonucleic acid}
		\acro{DNA}{deoxyribonucleic acid}
		\acro{ncRNA}{non-coding RNA}
		\acro{ncRPI}{ncRNA-Protein interaction}
	\end{acronym}
	
	\newpage
	\pagenumbering{arabic}
	\renewcommand{\thepage}{\arabic{page}}
	
	\section{Abstract}
	
	Ensemble methods describe the concept of aggregating algorithms into a higher level model. The way of aggregation heavily impacts the performance and general output of an algorithm. These different approaches are steadily researched and applied to many real world problems. Because of their variability and therefore large and diverse user base these methods will continue to be part of data science in the future. An overview and comparisons of different techniques yield insight into picking the most suitable algorithm to an individual problem and an informed researcher can avoid pitfalls when reading up on state-of-the-art technologies.
	
	%-------------- Einleitung
	\section{Introduction}
	
	The amount of data the modern world produces reaches newer heights everyday. The internet gave rise to a myriad of ways to communicate, create and analyse knowledge. This has brought new focus to the challenge of processing and assessing ever growing amounts of data. Not only size but also structure is a relevant factor in tackling these challenges. The curse of dimensionality describes situations where very few data points each consist of many more features, often ranging in thousands or even tens of thousands. Especially results in the field of bioinformatics face this problem on a regular basis. Algorithms need to be able to analyse this data not only correctly but efficiently. Machine learning (ML) offers many different approaches and has been a field of interest for modern day science quite some time now. While simple algorithms like the \ac{DT}\cite{morgan1963} and \ac{k-NN}\cite{fix1952} have been known for decades other methods have been invented and improved. To this day ensemble methods like random forests (RF) or many boosting algorithms are in use and of course the concept of a \ac{NN} is applied throughout science and in the economy. Figure \textbf{\ref{ensemble_paper}} shows the rise in released papers concerned with the 'ensemble learning' term. This work tries to give a brief explanation on how some of these algorithms work and what purposes there are for them today. At last there will be a critical comparison and conclusion about the findings.
	
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=1\textwidth]{ensemble_paper.png}
		\end{center}
		\caption{"The number of papers published in the core set of Web of Science from 1990 to 2019 for the topic of ‘ensemble learning’"\cite{yang2022}}
		\label{ensemble_paper}
	\end{figure}
	
	\section{Theoretical Basics}
	This chapter focuses on explaining the concepts of algorithms and techniques used to create and apply ensemble methods. These explanations are of course neither complete nor in-depth but rather an overview and a few choice examples made by the author.
	
	\subsection{Machine Learning}
	Techniques that make it possible for a computer to make assumptions about data by analyzing said data or other data from the same category fall under the modern day term \ac{ML}. These techniques usually stem from the field of statistical learning. A mathematical model is applied, also called fitted, to the data and then evaluated for its success to correctly predict something about the relevant data. This field of science is mostly split into two areas, supervised and unsupervised learning. In supervised learning the model is supplied with training data that is labeled, meaning its outcome variable is known. The model then tries to extract general rules that describe which combination of features in a data point lead to it producing said outcome variable. This rule is then applied to new data. The more accurate its predictions are, the better the model is regarded in its performance. In unsupervised learning the given data is not labeled and has no outcome variable. Instead the model tries to find out how the data is structured and make assumptions about groups or clusters of data points. The performance of unsupervised learning algorithms is not as simply measured as of supervised ones.\citep[p. 1]{Hastie2017}
	
	The techniques described in this work only focus on supervised learning. The outcome variable can come from a list of categories, which makes it a class label. The problem is then called a classification problem. If the problem is regressional the outcome variable is continuous, usually a real number. The outcome variable is also called the target.\citep[pp. 27-28]{Müller2017}
	
	In the process of fitting a model to data one hurdle to take is to avoid over- and underfitting. When the model takes into account the information of the training data often some part of it is retained to serve as test data to see how well it will perform on data it has not seen yet. However this performance can be altered by tuning the algorithm in a way so its prediction about this exact data set improves. This may often only yield a better performance on this specific data and shows no improvement in the real world application, or even worse, it will be less performant on future data. If the model is too closely fitted to the training and test data it is called overfitting.
	On the other hand, if the model generalizes too loosely based on the given data, it does not utilize the full scope of information supplied and may even do bad on the test data retained from the training data. This is called underfitting. More complex models tend to do better on test data, but are prone to overfitting, too simple a model will suffer from underfitting. An optimum exists between the two and is visualized in Figure \textbf{\ref{fitting}}.\citep[pp. 28-30]{Müller2017}
	
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=0.8\textwidth]{over_underfitting.png}
		\end{center}
		\caption{Over- versus underfitting and the ``sweet spot'' visualized.\citep[p. 31]{Müller2017}}
		\label{fitting}
	\end{figure}
	
	In the same realm of problems falls the bias-variance-tradeoff. A model has high bias when its predictions are consistent, but generalizes badly on data. These models tend to suffer from underfitting. High variance models do not make stable predictions, meaning it can not be trusted to predict the same on future data as it does on training data. Both cases can appear simultaneously which would be the worst case and in a best case both bias and variance would be nonexistent and the model would therefore only be making true predictions. In reality there is often a tradeoff to be made between the two.\citep[pp. 111-114]{Lopez2022}
	
	`` In explanatory modeling the focus is on minimizing bias to obtain the most accurate representation of the underlying theory. In contrast, predictive modeling seeks to minimize the combination of bias and estimation variance, occasionally sacrificing theoretical accuracy for improved empirical precision.''\citep[p. 293]{Shmueli2010}
	
	\subsection{Base learners}
	The ensemble in ensemble methods stems from the usage of combining many classifiers together in a way that tries to improve their gathered performance in comparison to a single classifier. This single classifiers are called base learners. Per se any classifier can be used as base learner, but often relatively simple algorithms are used.\citep{Yang2010} Sometimes they are also called ``weak learners'' and the ensemble together forms a ``strong learner''\citep[p. 137]{Ghavami2020}
	
	A popular choice of base learner in ensemble methods are \ac{DT}s. A \ac{DT} in itself parts the given data along a certain value for a certain feature into two groups. These groups make the new nodes and are themselves split again. The splits are chosen in a fashion to minimize impurity of the nodes. When faced with a classification problem this keeps going until all data in a node is of the same class. This node is a leaf and not split any further. If the target takes on continuous values the user has to define some value at which the splitting stops. After the tree is grown a technique is used called pruning, where overly deep trees are cut back. This is usually computational intensive. The tree assigns a prediction to every leaf either as the mean of the values if its facing regression or majority voting if facing classification. The user may influence the results of a tree by deciding the way of splitting the trees, the depth of the trees and by setting costs for misclassification in a leaf.\citep{Moisen2008}
	
	A special kind of \ac{DT} is a ``stump''. These are one-level \ac{DT}s that split given data into exactly two sets, therefore being an extremely weak learner, but an easily and quickly constructed one.\citep{Iba1992}
	
	\subsection{\nmu Variable \nmu Importance \nmu Measure}
	\label{variable importance measure}
	
	The simplest approach to judge a data split is the Misclassification error. It describes the proportion of class instances that would be labeled wrong, if a class label was applied to all data, for example through majority voting.\citep{Moisen2008}
	
	``Entropy is an information-theoretic measure of the ‘uncertainty’ contained in a training set, due to the presence of more than one possible classification.''\citep[p. 57]{Bramer2007} This characteristic of entropy can be used to judge the significance of classifying data along a feature or value for that feature. It is calculated by the following formula:
	
	\begin{center}
		$\mathlarger E =\sum\nolimits_{i=1}^K pi\log pi$
	\end{center}
	
	Entropy being denoted as \textit{E}, \textit{pi} being the proportion of instances in class \textit{i} of all classes \textit{K}. Entropy therefore takes on a value greater than or equal to zero, zero being the case, when all instances are of one class, and its maximum when there are equal amounts of instances across all possible classes. From this entropy we can calculate information gain based on the weighted difference between the old Entropy and the new on. It is weighted according to the amount of data that each new subset contains after the data is regrouped along the chosen feature and value therefore. Maximising information gain can give the most relevant feature to regroup data.\citep[pp. 57-61]{Bramer2007}
	
	A different approach that still computes importance based on impurity is the Gini index. It is to Gini Impurity what information gain is to Entropy. The formula for Gini Impurity \textit{G} is as follows:
	
	\begin{center}
		$\mathlarger G =\sum\nolimits_{i=1}^K pi (1 - pi)$
	\end{center}
	
	Using the same definitions for the mathematical variables as for entropy, the Gini index too is computed in a similar way to information gain by subtracting the new Gini impurity from the old one.\citep{Breiman1984} A variation to the Gini index takes into account the depth at which the variable is used to classify data, since more important variables tend to be used first\citep{chen2007}.
	Both information gain and the Gini index suffer from systemically preferring features with lots of categories because they have a higher chance to make significant impact at random. Continuous features are chosen over categorical ones for the same reason.\citep{Boulesteix2012}
	
	The permutation variable importance measures a variables importance by its effect on the prediction. Each variables value is permutated randomly on its own in held out data from the training set. This disrelates the variable from the target and the difference between the misclassification of the model with unpermutated and permutated data gives a rate at which the chosen variable impacts the prediction.\citep{Breiman2001} This technique does not suffer from the same bias as the Gini index or information gain for continuous or high frequency variables but demands much higher computational effort\citep{Boulesteix2012}. A modified version of this is the hold-out permutation variable importance measure where a second model is built with the held out data and the observed variable left out. The difference in performance to the original model then states the variable importance.\citep{Janitza2018}
	
	These examples have been choices for classification problems. For regression a weighted variance approach can be used. It determines impurity of data by the sum of squares and the improvement when choosing a certain variable for splitting data by its impurity reduction in the difference to the weighted sum of squares of the data subsets.\citep{Ishwaran2015}
	
	More ways to compute variable importance measures are provided in \cite{Mingers1989} and \cite{Nembrini2018}.
	
	\subsection{Ensemble methods}
	
	Ensemble methods combine base learners in specific ways to improve performance. All techniques share some principles but they each have their own strengths and applications. Three popular ensemble methods are \textbf{\lnameref{Bagging}}, \textbf{\lnameref{Boosting}} and \textbf{\ac{RF}s}, even though they all have even more specialized variants. The basic concepts are visualized in Figure \textbf{\ref{ensembles}}. Beyond that \textbf{\lnameref{stacking}} tries to deduce wether a set of heterogeneous base learners improves prediction accuracy.
	
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=1\textwidth]{ensembles.png}
		\end{center}
		\caption{Basic concepts of bagging, boosting and \ac{RF}s respectively \citep{Yang2010}.}
		\label{ensembles}
	\end{figure}
	
	\subsubsection{\nmu Bagging}
	\label{Bagging}
	
	Bagging describes the concept of building several base learners and aggregating their predictions through majority vote when the target is a class or averaging when it is a numerical value. Every base learner is built on a different subset from the original data set generated through bootstrapping. A bootstrap data set will omit certain data points and multiply others, therefore supplying different sets of data without creating entirely new data itself. This also creates a test set, since the data points not chosen when picking data points with replacement can be used for the test set. This is often referred to as out-of-bag samples. Together bootstrap aggregating forms the acronym bagging. A prerequisite for this work is the perturbability of the given data, meaning bootstrapping the data will change the base learners outcome.\citep{Breiman1996}
		
	\subsubsection{\nmu Random subspace method}
	\label{Random subspace method}
	
	When there are large numbers of features present the random subspace method has been proven to thrive\citep[p. 1055]{jukes2018}. Like any ensemble method it grows a set of base learners but only a random subset of features are used for each individual learner. This leads to every learner ignoring some features while focusing more on others. Like \textbf{\lnameref{Bagging}} this algorithm aggregates the results of its base learners into a single output. The random subspace division does not have to happen only once for every base learner but can also be applied to its levels, for example every node of a \ac{DT} could be built around a different random subspace.\citep{ho1998}
	
	\subsubsection{Random Forests}
	
	\ac{RF}s have been introduced first by \cite{Breiman2001} and combine the concepts of \textbf{\lnameref{Bagging}} and \textbf{\lnameref{Random subspace method}} while being able to outperform both of them. They use \ac{DT}s as base learners hence the name. They are very popular among novice computer scientists since they are easy to understand and only have few parameters to tune. Namely these parameters are the amount of trees, size of the random subsets for each tree node to choose a split from and the size of a leaf. There more trees a forest consists of the better while more trees will not lead to overfitting.\citep{biau2016} A typical value for the subset size is $\sqrt{p}$ for regression forests or $p/3$ for classification forests where $p$ is the number of features the data possesses, i.e. the dimensionality\cite{Nembrini2018}. Node size is usually five\citep{Breiman2001}. When the out-of-bag samples provided by bagging are used for error calculation the mean of all components stands for the out-of-bag error of the whole forest model. Lastly \textbf{\lnameref{variable importance measure}} is an important feature which heavily influences the outcomes of a random forest and choosing a better measure can improve performance.

	\subsubsection{\nmu Boosting}
	\label{Boosting}
	
	While research produced many different variants of boosting algorithms, AdaBoost by \cite{freund1997} is still the most prominent. Like Bagging it combines the performance of many weak learners, which in this case a barley better than guessing randomly, into a strong learner with much better performance. But unlike Bagging it does this iteratively by feeding the output from one learner to the next. Every weak learner makes its predictions on test data and these predictions are used to produce a new data set weighted in favor of wrong predictions. This forces the next learner to be built around the predictions that have been wrong in hopes of predicting them right. This is repeated a number of times, improving performance with each iteration.
	
	Originally developed for binary class cases, so the weak learner only has to assume a target takes on one of two instances, many derivatives exist to extend its purpose to multiclass problems\cite{schapire1999}. AdaBoost.M2 splits the multiclass variable into as many binary class variables as needed\cite{freund1997}. AdaBoost.MH tests the hypothesis if the data is classified as one of the labels or any of the other labels, making it thus a binary class case\cite{schapire1998}.

	
	\subsubsection{Neural Networks}
	
	"In a neural network, learning algorithms adjust the connection weights between
	neurons. An early algorithm was proposed by Hebb (1949) and is known as the
	Hebbian learning rule: the weight between two neurons gets reinforced if the two
	are active at the same time—the synaptic weight effectively learns the correlation
	between the two neurons." \citep[p. 192]{alpaydin2021}
	
	Ensembling more than one \ac{NN} can yield to an improved performance in respect to each single network. The classical approach of training many networks and choosing the one with the best classification can be outperformed. The difference in the single networks outputs stems from the optimization problem of finding a global minimum in a space with many local minima where many different search methods and parameters exist hence producing outcomes depending on the applied methods. Aggregating the outputs of all networks may improve accuracy drastically compared to one alone.\citep{hansen1990} The way of ensembling can be chosen from classical ensemble approaches like bagging or boosting\citep{zhou2002}. Alternative aggregation methods are a research field itself, for example dynamically weighting each networks vote during the classification at hand\citep{jimenez1998}, by reducing redundancy in the learned models through principal component regression\citep{merz1996}, by linearly combining them along optimized weights\cite{ueda2000} or using stacked generalization based on the bias of each component model\citep{wolpert1992}.
	
	\subsubsection{\nmu Stacking}
	\label{stacking}
	
	While \textbf{\lnameref{Bagging}} and \textbf{\lnameref{Boosting}} usually use a homogeneous set of base learners stacking is an approach that builds a model based on different models. It was introduced in 1992 by \citep{wolpert1992}. While these base models are fitted to the training data, their predictions on validation data is used as an input for higher-class learner, also called the meta-learner, which is then used to make the actual prediction of the final model.\citep{mellouk2009} While aggregation methods that use voting like \textbf{\lnameref{Bagging}} or \textbf{\lnameref{Boosting}} apply no learning to the aggregation part of their algorithm stacking does so and there has two levels of learning\citep{dvzeroski2002}. Figure \textbf{\ref{stacking_fig}} supplies a schematic to the process of stacking. 
	
	Stacked layers of \ac{NN}s are considered \ac{DL}\citep[p. 288]{dong2018}. While the terms "stacking" and "blending" are sometimes used synonymously, "stacking" refers to using out-of-fold predictions by the base learners on the training data and "blending" separates a hold-out set from the training data which is used for base learner prediction\citep[p. 209]{geron2022}.
	
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=1\textwidth]{stacking.png}
		\end{center}
		\caption{Basic concept of stacking.}
		\label{stacking_fig}
	\end{figure}
	
	\section{Practical applications}
	
	While the concepts of \ac{ML} are easily and readily applied to a broad spectrum of problems ranging from finance, language processing to medicine this section focuses on its applicability to bioinformatics\citep[p. 1]{Hastie2017}. Even within this field of science these are merely examples and by far not an exhaustive list of what exists. Theoretical comparisons of ensemble methods on data supplied by \citep{dua2019} have been performed in \cite{verma2017}.
	
	\subsection{Microarrays}
	
	The emergence of Microarray technologies paved the way for genetic research that was not feasible before, either because it was not possible or to expensive. With these new possibilities came new challenges because huge amounts of data needed to be processed and analysed.\citep{brown1999}
	
	Comparing several \ac{ML} methods to classical approaches and other models on different datasets yields that ensemble methods like \ac{RF} and boosting are very well capable of classifying cancer cells in different samples and despite a high amount of dimensions. Row two to five in Table \textbf{\ref{microarray_data}} show the error rates for different ensemble methods respectively. Dimensionality in these data sets is well in the thousands while sample size is mostly below 100.\citep{Lee2005}
	
	\begin{table}[h]
		\begin{center}
			\includegraphics[width=1\textwidth]{microarray_data.png}
		\end{center}
		\caption{Error rates of different \ac{ML} methods on public datasets. C signifies the size of the target variable. \cite{Lee2005}.(truncated and modified)}
		\label{microarray_data}
	\end{table}
	
	\subsection{Severity of Parkinson Disease}
	
	People suffering from Parkinson Disease show symptoms like shaking, reduced movability and other motorical impediments. After Alzheimer's it is the second most common nervous degenerative disease in the world. Since there is a Unified Parkinson’s Disease Rating Scale that can be taken as measure of severity which is deducible from a patients voice recordings the situation supplies both data and a way of judging it. Currently there is no cure for Parkinson's Disease available so assessing the condition of an individual patient is crucial to supplying him with the appropriate care to maintain the best possible quality of life for him.\citep{halawani2012}
	
	\subsection{Mass spectrometry}
	
	Diseases are not only affected by genetics but also express their dynamic state in body fluids. The protein composition of these fluids can be analysed by mass spectrometry but the produced data typically suffers from the curse of high dimensionality, i.e. there are few samples containing a high number of features since fluids contain many different proteins. The challenge is to extract the impact of a certain composition on the state or presence of a disease to help with its diagnosis or treatment.\citep{Geurts2005}
	
	A study examined surface-enhanced laser desorption/ionization time of flight mass spectrometry datasets from patients with \ac{RA} and \ac{IBD} in hopes of finding biomarkers specific to a each disease and possibly aid diagnosis by identification of these found biomarkers. Variable importance measures supplied by ensemble methods offer a way of identifying possibly related biomarkers. Reduction of entropy was used in this study either by calculating the sum in forests or weighted sum in a boosting algorithm. Since mass spectrometry data is very noisy it was preprocessed by two different methods. Discretization by a roughness parameter \textit{r} specified by the authors and peak detection supplied by the used software. Both methods were applied separately to test their effect on the algorithms performance. Table \textbf{\ref{mass_spectrometry_attributes}} shows that both \ac{ML} algorithms find a biomarker to be the most relevant which is completely omitted by the peak detection method of the software.\citep{Geurts2005}
	
	\begin{table}[h]
		\begin{center}
			\includegraphics[width=1\textwidth]{mass_spectrometry_attributes.png}
		\end{center}
		\caption{Comparison of variable importance measured by single CART(left) and boosting(right) on both datasets with two different preprocessing methods(r and peak)\citep{Geurts2005}.}
		\label{mass_spectrometry_attributes}
	\end{table}
	
	This missing attribute could be important and it missing could be the reason for accuracy loss seen in peak detection when comparing table \textbf{\ref{mass_spectrometry_data_peak}} and table \textbf{\ref{mass_spectrometry_data_r}}.
	
	\begin{table}[h]
		\begin{center}
			\includegraphics[width=1\textwidth]{mass_spectrometry_data_peak.png}
		\end{center}
		\caption{Accuracy of different \ac{ML} algorithms on two datasets with peak detection and alignment applied beforehand\citep{Geurts2005}.}
		\label{mass_spectrometry_data_peak}
	\end{table}
	
	\begin{table}[!h]
		\begin{center}
			\includegraphics[width=1\textwidth]{mass_spectrometry_data_r.png}
		\end{center}
		\caption{Accuracy of different \ac{ML} algorithms on two datasets with the best discretization \citep{Geurts2005}.}
		\label{mass_spectrometry_data_r}
	\end{table}
	
	\newpage
	
	\subsection{Non-coding RNA interaction with proteins}
	
	While the information for proteins is encoded in the \ac{DNA}, the protein encoding parts of the human genome only amount to less than two percent\citep{omenn2021}. The other areas are still relevant to the organism since they play a part in the regulation of gene expression and cellular processes. They often produce \ac{ncRNA} which for example plays an important role in the immune response to viral infection or cancer development.\citep{schmidt2017} Some of these \ac{ncRNA}s function through \ac{ncRPI}s and are found throughout the organism and many different types of tissue\citep{yi2020}. They can also play a crucial role in the emergence of diseases. Examples include but are not limited to Alzheimer's Disease\citep{faghihi2008}, hearing loss\citep{lewis2009} and cancer development\citep{shahrouki2012}. Identifying these \ac{ncRPI}s is an active field of research and can be tackled with an \ac{ML} approach. Application of \ac{RF}s has shown some success in finding \ac{ncRPI}s in yeast based on physical and chemical properties, localization and known interactions of associated proteins, \ac{RNA} and \ac{DNA}\citep{pancaldi2011}. Similarly \ac{RF}s where applied solely to the sequence data of the \ac{RNA} and proteins to predict interactions\citep{muppirala2011}. Long \ac{ncRNA} has been successfully analyzed using a stacked ensemble of \ac{NN}s\citep{fan2019}. IPMiner is a method using stacked \ac{RF}s and \ac{NN}s to find  hidden high-level features in \ac{ncRPI}s\citep{pan2016}.
	
	\section{Discussion}
	
	Ensemble methods have been object to performance comparisons for quite some time. They almost always outperform their base classifiers like \ac{DT}s and depending on how the data is structured they are within the top classifiers in respect to error rate. Table \textbf{\ref{microarray_data}} shows the error rates of \ac{RF} and boosting algorithms to classical methods, \ac{NN}s and \ac{SVM}s. The ensemble methods outperform classical methods and are on par with \ac{SVM}s in some cases.\citep{Lee2005}
	
	One key problem when judging a presented \ac{ML} technique is the tuning of parameters to fit the solvable question at hand. While some algorithms require more tuning than others there is no algorithm that works perfect without adjusting its parameters. When faced with statistics about error rates and accuracy one must always ask the question how finely tuned each of the competitors was to the underlying data. When the improvement of a techniques accuracy over another is only a slight one this must be examined even more so because these seeming advantages might just be the result of carefully adjusting the method by hand and without a notice by the authors it is not even reliably reproducible.
	On the other hand, as mentioned before, any algorithm needs tuning. Therefore there is a fine line to walk between giving new technologies a fair chance to be applied correctly and tilting the results to its favor.
	
	\ac{DL} has been on the rise as a technique to tackle many different problems in questions of artificial intelligence. This has been competition to traditional \ac{ML} algorithms but there is also considerable overlap to be found. \ac{DL} is a powerful yet costly way to analyse data since the training of a \ac{NN} often comes with huge amounts of tunable parameters. Training more than one \ac{NN} for the purpose of assembling multiplies those costs. Advances in high-performance computing resources have made the uses possible and viable, outperforming some traditional \ac{ML} methods in cases where access to these powerful resources is given. But \ac{DL} and ensemble methods do not have to be adversaries. Incorporating \ac{DL} into ensemble methods or vice versa has been a growing focus in science for some years. It has been applied successfully to many scientific questions but the problem of efficiency is still its biggest challenge. Reducing the computational effort of this effective combination of \ac{ML} methods will certainly decide wether or not ensemble deep learning can compete with other approaches.\cite{yang2022} For example this has been tried by finding local minima in data before the training of the \ac{DL} learning structure based on each minimum separately\cite{yang2021}. 
	
	
	\section{Conclusion}
	
	Ensemble methods have been proven to be a valid alternative to simple base learners. While some approaches are complicated and require extensive knowledge in mathematics or a related field, others, for example \ac{RF}, are easily understood even by novice data scientists. This spectrum of algorithms gives a broad audience access to the power of \ac{ML} and effective methods for data analysis without overwhelming users from different fields and curtailing the abilities of experienced or trained experts. Still there needs to be made an informed decision when to use which technique. While the advent of \ac{NN} technologies has seen many applications there is always the question wether the training of a \ac{NN} is warranted or even relevant to the problem. As with any \ac{ML} method parameters must be tuned but with that in mind any comparison between performances needs to be taken with a grain of salt if no details of parametrization are specified since this can impact an algorithms accuracy heavily.
	
	As \ac{ML} is and stays a hot topic throughout modern day society, ensemble methods will draw many scientists attention to solve their problems. This will lead to newer findings and research in trying to improve techniques in efficiency and accuracy.
	
	%-------------- Literaturverzeichnis
	\newpage
	\addcontentsline{toc}{section}{Literature}
	\bibliography{Sources.bib}
	
	
	%-------------- Anhang
	\newpage
	\appendix
	
	
	\clearpage
	
	\section*{Affidavit}
	I hereby declare that I have written this thesis independently and without outside help and that I have not used any other aids than those indicated.\\
	In particular, I certify that I have marked all verbatim and semantic copies from other works as such.\\
	
	
	\vspace{3cm}
	\begin{flushleft}
		\begin{figure}[!h]
			\hspace{9cm}\includegraphics[width=0.5\textwidth]{signature.png}
		\end{figure}
		Gießen, \today\hspace{6.76cm} Signature
	\end{flushleft}
\end{document}

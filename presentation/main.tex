\documentclass{beamer}
\usetheme[subsectionpage=progressbar]{metropolis}

%---------------------------------------Pakete
\usepackage{url}
\usepackage[absolute,overlay]{textpos}
\usepackage[english]{babel}
\usepackage[style=english]{csquotes}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{scrextend}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{tikz}
\usepackage{pgfpages}
% nächste Zeile auskommentieren wenn man eine normale PDF haben möchte
\setbeameroption{show notes on second screen}
\captionsetup[figure]{font=scriptsize}
\usepackage[backend=bibtex, style=nature, citestyle=numeric-comp]{biblatex}
\addbibresource{sources.bib}

\definecolor{thmgreen}{rgb}{0.5, 0.73, 0.14}
\definecolor{lightgray}{rgb}{0.95, 0.95, 0.95}
\newcommand{\myitem}{\item[\textcolor{thmgreen}{$\blacktriangleright$}]}
\lstset{breaklines=true, basicstyle={\ttfamily\small}, frame=single, backgroundcolor=\color{lightgray}, rulecolor=\color{thmgreen}, moredelim=**[is][\color{thmgreen}]{@}{@}}

% für die "das ist ein Link" Pfeile:
\newcommand{\ExternalLink}{%
    \tikz[x=1.2ex, y=1.2ex, baseline=-0.05ex]{%
        \begin{scope}[x=1ex, y=1ex]
            \clip (-0.1,-0.1)
                --++ (-0, 1.2)
                --++ (0.6, 0)
                --++ (0, -0.6)
                --++ (0.6, 0)
                --++ (0, -1);
            \path[draw,
                line width = 0.5,
                rounded corners=0.5]
                (0,0) rectangle (1,1);
        \end{scope}
        \path[draw, line width = 0.5] (0.5, 0.5)
            -- (1, 1);
        \path[draw, line width = 0.5] (0.6, 1)
            -- (1, 1) -- (1, 0.6);
        }
    }
%---------------------------------------Titeleinstellungen
\title{\Large Ensemble methods in Bioinformatics}
\subtitle{\large Seminar Bioinformatics}
\date{{\large 16. Febuary 2023}}
\author{\large Georg Grünert}
\institute{{\large Technische Hochschule Mittelhessen}}

\begin{document}
%---------------------------------------Titel
\setbeamercolor{background canvas}{bg=white}
\setbeamercolor{title}{bg=thmgreen}
\setbeamercolor{structure}{fg=thmgreen}
\setbeamercolor{progress bar}{ fg=thmgreen }
\setbeamercolor{title separator}{ fg=thmgreen }
\setbeamercolor{progress bar in head/foot}{ fg=thmgreen }




\maketitle

\begin{frame}{Structure}
    \thispagestyle{empty}
    \setcounter{tocdepth}{1}
    \tableofcontents
    \note[]{
    only some theory

    choice examples}
\end{frame}

\section{Theoretical Basics}


\subsection{Machine Learning}
\note[]{machine learning in generell}

\begin{frame}{Machine Learning}
    \note[]{
    statistical methods

    linear regression analyses from gauß

    other types of learning
    }
    \begin{itemize}
     \myitem uses statistical methods to analyze data and make assumptions about it
     \pause
     \myitem roots date back to century old data analyses tools
     \pause
     \myitem \textbf{supervised} vs. unsupervised learning
    \end{itemize}
\end{frame}

\begin{frame}{Machine Learning - Examples}
    \begin{itemize}
     \myitem predicting the hospitalization of a patient based on demographical and patient data\cite{Hastie2017}
     \myitem identify handwritten text from a digitised photo\cite{Hastie2017}
     \myitem calculate the market value of real estate depending on location, square footage and year of construction
     \myitem group customers according to their purchase behaviour
    \end{itemize}
\end{frame}

\begin{frame}{Machine Learning - Examples}
\note[]{focus on supervised learning}
    \begin{itemize}
     \myitem predicting the hospitalization of a patient based on demographical and patient data\cite{Hastie2017} - \textbf{supervised}
     \myitem identify handwritten text from a digitised photo\cite{Hastie2017} - \textbf{supervised}
     \myitem calculate the market value of real estate depending on location, square footage and year of construction - \textbf{supervised}
     \myitem group customers according to their purchase behaviour - \textbf{unsupervised}
    \end{itemize}
\end{frame}

\begin{frame}{Supervised Learning - Example}
\note[]{training data is labeled; target is known and given to model

model deduces a ``rule'' for new data

test data has known target, but not supplied to model

error rate measurement of quality}
    \begin{figure}
        \begin{center}
            \includegraphics[width=1\textwidth]{figures/model.png}
            \caption{Example for supervised learning in plant prediction\cite{crossa2017}}
        \end{center}
    \end{figure}
\end{frame}

\begin{frame}{Machine Learning - Data}
\note[]{m rows

n columns

Y is discrete/continous; classes vs real numbers

transform regression to classification problem

use native algorithm for regression}
    \begin{center}
        \begin{tabular}{||c c c c c c c c||}
            \hline
            Sample & \(X_1\) & \(X_2\) & ... & \(X_i\) & ... & \(X_n\) & Y\\ [0.5ex]
            \hline\hline
            \(S_1\) & value & value & ... & value & ... & value & value/label\\
            \hline
            \(S_2\) & value & value & ... & value & ... & value & value/label\\
            \hline
            \(S_m\) & value & value & ... & value & ... & value & value/label\\ [1ex]
            \hline
        \end{tabular}
    \end{center}
    \begin{itemize}
     \pause
     \myitem rows represent \textit{m} sample
     \pause
     \myitem columns represent \textit{n} features
     \pause
     \myitem Y is the target variable, known for training data, relevant output of the model
    \end{itemize}
\end{frame}

\begin{frame}{Machine Learning - Data - Example}
\note[]{binary classification problem

what treatment for gause?

data may be insufficient

model cannot account for unknown factors}
    \begin{center}
        \begin{tabular}{||c c c c c||}
            \hline
            Patient & Weight & Height & Stent & Treatment\\ [0.5ex]
            \hline\hline
            M., Broder & 112 & 185 & Yes & Operation\\
            \hline
            L., Hastie & 80 & 175 & Yes & Medication\\
            \hline
            G., Gause & 75 & 160 & No & ?\\ [1ex]
            \hline
        \end{tabular}
    \end{center}
    \begin{itemize}
     \myitem rows represent \textit{m} sample
     \myitem columns represent \textit{n} features
     \myitem Y is the target variable, known for training data, relevant output of the model
    \end{itemize}
\end{frame}

\begin{frame}{Machine Learning - Models}
\note[]{called bias-variance-tradeoff}
    \begin{itemize}
     \myitem machine learning models analyse features and try to deduce the target
     \pause
     \myitem they have to account for as much information as possible but be flexible enough to allow new data to be different
    \end{itemize}
\end{frame}

\subsection{Bias-variance-tradeoff}

\begin{frame}{Bias-variance-tradeoff}
\note[]{biased model has simple rule and judges by it; not taking into account nuances of info

high variance model will even take noise into account}
    \begin{columns}
     \column{0.5\textwidth}
     \centerline{\textbf{{\Large Bias}}}
     \begin{itemize}
      \myitem has a predetermined ``opinion'' on data
      \myitem high bias will do poorly in general
     \end{itemize}
     \mbox{}\\
     \pause
     \column{0.5\textwidth}
     \centerline{\textbf{{\Large Variance}}}
     \begin{itemize}
      \myitem accounts for the spread of data
      \myitem high variance will do well on training data and poorly on test data
     \end{itemize}
    \end{columns}
\end{frame}

\begin{frame}{Base-variance-tradeoff}
\note[]{too simple, information is lost}
 \begin{center}
    \begin{figure}
        \includegraphics[width=0.7\textwidth]{figures/bias.png}
        \caption{High bias example\cite{Hastie2017}}
    \end{figure}
  \end{center}
\end{frame}

\begin{frame}{Base-variance-tradeoff}
\note[]{new datapoint close to outlier may be misclassified}
 \begin{center}
    \begin{figure}
        \includegraphics[width=0.7\textwidth]{figures/variance.png}
        \caption{High variance example\cite{Hastie2017}}
    \end{figure}
  \end{center}
\end{frame}

\begin{frame}{Base-variance-tradeoff}
\note[]{possibility of good fit}
 \begin{center}
    \begin{figure}
        \includegraphics[width=0.7\textwidth]{figures/good_fit.png}
        \caption{Well fitted model\cite{Hastie2017}}
    \end{figure}
  \end{center}
\end{frame}

\begin{frame}{Base-variance-tradeoff}
\note[]{also called underfitting and overfitting

overfit model is overly fit to training data

underfit model is to loosely fit to training data

accuracy on training data}
 \begin{center}
    \begin{figure}
        \includegraphics[width=1\textwidth]{figures/tradeoff.png}
        \caption{Underfitting vs Overfitting\cite{Mueller2017}}
    \end{figure}
  \end{center}
\end{frame}

\subsection{Ensemble methods}

\begin{frame}{Ensemble methods}
\note[]{stumps covered shortly

base learners are classifiers themselves

classifier examples: knn, linear models, svms

we want unstable classifiers;

input change -> output change = unstable

many DIFFERENT opinions

many different types but I'll explain two}
 \begin{itemize}
  \myitem ``Ensemble'' because many base classifiers are combined together to create a better classifier
  \pause
  \myitem base classifiers can be very weak(stumps) or even strong like an ensemble itself(Meta-ensemble)
  \pause
  \myitem stable classifiers are \textbf{not} as useful als unstable classifiers
  \pause
  \myitem many different ways and variants how to combine the base classifiers
 \end{itemize}
\end{frame}

\begin{frame}{Ensemble methods - Decision trees}
 \note[]{some may recognize this but it's still funny

 tea time}
 \begin{figure}
  \begin{center}
   \includegraphics[height=0.8\textheight]{figures/christmastree.png}
   \caption{Christmas Tree\cite{xkcd}}
  \end{center}
 \end{figure}
\end{frame}

\begin{frame}{Ensemble methods - Decision trees}
 \note[]{you know decision trees}
 \begin{itemize}
  \myitem split data at every node along a certain feature
  \pause
  \myitem maximize homogeneity in the two daughter nodes
  \pause
  \myitem leafs are assigned a target value
  \pause
  \myitem new data is filtered along node split rules and assigned the target value of the leaf it lands in
  \pause
  \myitem \textbf{unstable} since small changes in data can return a very different tree
 \end{itemize}
\end{frame}

\begin{frame}{Ensemble methods - Decision trees}
 \note[]{two feature data

 different data may change quadrants completely

 procedurally generated, so first split affects later splits

 finding and judging good splits takes time; different approaches possible}
 \begin{figure}
  \begin{center}
   \includegraphics[width=1\textwidth]{figures/decisiontree.png}
   \caption{Decision boundary of tree with depth 2(left) and corresponding tree(right)\cite{Mueller2017}}
  \end{center}
 \end{figure}
\end{frame}

\begin{frame}{Ensemble methods - Decision trees}
 \note[]{one split trees -> stumps

 weak learners -> barely better than random guessing

 easy to build}
 \begin{figure}
  \begin{center}
   \includegraphics[width=1\textwidth]{figures/stump.png}
   \caption{Decision boundary of tree with depth 1(left) and corresponding tree(right)\cite{Mueller2017}. This is called a ``stump''.}
  \end{center}
 \end{figure}
\end{frame}

\begin{frame}{Ensemble methods - Random Forests}
 \note[]{introduced 2001 by Leo Breiman

 amount of trees -> more are better

 amount of features per subset}
 \begin{itemize}
  \pause
  \myitem combination of Bagging and the Random subspace method
  \pause
  \myitem Bagging itself being a combination of \textbf{B}ootstrap and \textbf{Agg}regat\textbf{ing}
  \pause
  \myitem easily built by inexperienced users because of few parameters
  \pause
  \myitem easy to parallelize because trees are grown independently
 \end{itemize}
\end{frame}

\begin{frame}{Ensemble methods - Random Forests}
\note[]{Bootstrap creates subsets with duplicates

each tree has own data set and left out data set

each node is only given subset of features to decide from

which variable is chosen depends on variable importance measure

trees are grown sufficiently deep

majority voting or mean calculation -> aggregation

out of bag error is good way to measure quality

variable importance measure can tell important features from unimportant ones}
 \begin{center}
    \begin{figure}
        \includegraphics[height=0.65\textheight]{figures/basic_rf.png}
        \caption{Concept of a Random Forest\cite{Lopez2022}(modified).}
    \end{figure}
 \end{center}
\end{frame}

\begin{frame}{Ensemble methods - AdaBoost}
\note[]{stumps are barely better than guessing}
    \begin{itemize}
        \myitem \textbf{Ada}ptive \textbf{Boost}ing
        \pause
        \myitem uses stumps as base learners
        \myitem stumps are grown procedurally, each one using the output of its predecessor as input
        \pause
        \myitem output data is weighted towards misclassified data
        \myitem vote is weighted towards prediction quality
    \end{itemize}
\end{frame}

\begin{frame}{Ensemble methods - AdaBoost}
    \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{figures/adaboost.png}
        \caption{Concept of Adaboost\cite{Yang2010}(modified).}
    \end{figure}
\end{frame}

\section{Practical applications}

\begin{frame}{Practical applications - Microarray data}
\note[]{analyse few samples with gene expressions

cancer or not, which kind of cancer

columns are gene expressions

curse of dimensionality

lots of noise}
\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{figures/microarry.png}
    \caption{Schematic illustration for gene expression of microarray data\cite{Qi2012}.}
\end{figure}
\end{frame}

\begin{frame}{Practical applications - Microarray data}
\note[]{Boosting and Random Forest outperforming classical algorithms

sometimes on par with neural networks and svms

1. Leukemia (LEU)Leukemia dataset composed of 3571 gene expressions in three classes

2. Lymphoma (LYM)Among 96 samples, we took 62 samples of 4026 genes in threeclasses (B-CLL-11, FL-9, and DLBCL-42).

3. NCI 60 (NCI60)The full dataset composedof 60 samples and 9703 genes. 

4. Colon cancer(COLON)We used this gene expression data collected with size of 62samples and 2000 genes.

5. Lung cance r(LUNG)We selected 73 samples (Adeno-41, Normal-6, and Squamous-17,Small cell-5, Large cell-4) and 918 genes.

6. Small round blue cell tumor (SRBCT)size of 63 samples and 2308 genes.

7. YeastGene The data matrix consists of 2467 genes by 79slides.}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/microarray_data.png}
    \caption{Error rates of different ML Algorithms applied to seven public datasets\cite{Lee2005}(modified).}
\end{figure}
\end{frame}

\begin{frame}{Practical applications - Mass spectrometry}
\note[]{somewhat similar

proteine profiles of body fluid samples

dnyamic state of diseases instead of genetic factors}
\begin{itemize}
    \pause 
    \myitem similar data format to microarrays
    \pause
    \myitem instead of gene expression mass to charge ratios are measured for proteines in a sample
\end{itemize}
\end{frame}

\begin{frame}{Practical applications - Mass spectrometry}
\note[]{rheumatoid arthritis (RA) and inflammatory bowel diseases (IBD)

infected, healthy and different disease; correctly identifying

ET = Extratrees, even more randomized by authors

outperforming in RA and coming close in IBD to SVMs}
\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{figures/mass_spectrometry_data.png}
    \caption{Error rates of different ML algorithms on two datasets\cite{Geurts2005}.}
\end{figure}
    
\end{frame}

\begin{frame}{Practical applications - Gene association study}
\note[]{single nucleotide polymorphisms

small changes in dna; can be associated with diseases or not; often in combination with others}
    \begin{itemize}
        \myitem SNPs - single nucleotide polymorphisms
        \pause
        \myitem genotype of thousands of SNPs can affect diseases
    \end{itemize}
\end{frame}

\begin{frame}{Practical applications - Gene assocation study}
\note[]{
adaptive neuro-fuzzy inference system

neural networks become harder to compute with more input layers

random forests can tackle this}
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/snp.png}
    \caption{Classifcation of circa 2000 SNPs by RF, ANFIS and available software\cite{barenboim2008}.}
\end{figure}
\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion}
Ensemble methods offer a wide variety of algorithms that can cater to different aspects of data and user experience. Because it offers easily understood variants for data science novices and opportunities to be modified in drastic ways they will probably be used and researched for a long time.    
\end{frame}

\section{Sources}

% \addcontentsline{toc}{section}{Sources}
\begin{frame}[allowframebreaks]{Sources}
\printbibliography[heading=none]
\end{frame}

\section{Discussion}

\begin{frame}{Discussion}
\note[]{according to breiman adaboost with decision trees as base learners

up for discussion

the man fame though}
    What do you think is the best out-of-the-box ensemble?
\end{frame}

\appendix
%\include{appendix.tex}

\end{document}

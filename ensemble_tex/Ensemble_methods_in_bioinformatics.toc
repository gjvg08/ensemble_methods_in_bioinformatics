\babel@toc {english}{}\relax 
\contentsline {section}{List of Figures}{II}{section*.2}%
\contentsline {section}{List of Tables}{III}{section*.5}%
\contentsline {section}{List of Acronyms}{IV}{section*.8}%
\contentsline {section}{\numberline {1}Abstract}{1}{section.1}%
\contentsline {section}{\numberline {2}Introduction}{1}{section.2}%
\contentsline {section}{\numberline {3}Theoretical Basics}{1}{section.3}%
\contentsline {subsection}{\numberline {3.1}Machine Learning}{1}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Base learners}{3}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Variable Importance Measure}{4}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Ensemble methods}{5}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Bagging}{5}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}Random subspace method}{6}{subsubsection.3.4.2}%
\contentsline {subsubsection}{\numberline {3.4.3}Random Forests}{6}{subsubsection.3.4.3}%
\contentsline {subsubsection}{\numberline {3.4.4}Boosting}{7}{subsubsection.3.4.4}%
\contentsline {subsubsection}{\numberline {3.4.5}Neural Networks}{7}{subsubsection.3.4.5}%
\contentsline {subsubsection}{\numberline {3.4.6}Stacking}{8}{subsubsection.3.4.6}%
\contentsline {section}{\numberline {4}Practical applications}{9}{section.4}%
\contentsline {subsection}{\numberline {4.1}Microarrays}{9}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Severity of Parkinson Disease}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Mass spectrometry}{9}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Non-coding RNA interaction with proteins}{11}{subsection.4.4}%
\contentsline {section}{\numberline {5}Discussion}{12}{section.5}%
\contentsline {section}{\numberline {6}Conclusion}{13}{section.6}%
\contentsline {section}{Literature}{14}{section.6}%
